import { TestBed } from '@angular/core/testing';

import { MapMarkersService } from './mapMarkers.service';

describe('MapMarkersService', () => {
  let service: MapMarkersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MapMarkersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
