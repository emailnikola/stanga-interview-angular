import { Component, OnInit } from '@angular/core';
import { MapMarkersService } from './services/mapMarkers.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'full-stack-challenge';
  latitude: number;
  longitude: number;
  zoom = 17;
  bounds = null;
  mapMarkers = [];
  properties = [];

  constructor(private mapService: MapMarkersService) {}

  ngOnInit() {
    this.mapService.mapMarkersAPI().subscribe(
      data => {
        this.properties = data.results.items;
        this.setMapMarkers();
        this.setMapPosition();
      }, 
      err => {
        console.log('err', err)
      }
    );
  }
  
  setMapMarkers() {
    this.properties.forEach(property => {
      let tempMarker: any = {};
      tempMarker['latitude'] = property.position[0];
      tempMarker['longitude'] = property.position[1];
      tempMarker['active'] = false;
      tempMarker['iconUrl'] = "/assets/images/marker-default.png"
      tempMarker['iconUrlActive'] = "/assets/images/marker-active.png"
      this.mapMarkers.push(tempMarker);
    });
  }

  setMapPosition() {
    let tempLatitude = 0
    let tempLongitude = 0
    this.properties.forEach(property => {
      tempLatitude += property.position[0]
      tempLongitude += property.position[1]
    })
    this.latitude = tempLatitude / this.properties.length
    this.longitude = tempLongitude / this.properties.length
  }

  setActiveMarker(index) {
    this.mapMarkers.forEach(marker => {
      if(marker.active) {
        marker.active = false;
      }
    })
    this.mapMarkers[index].active = true;
  }

  setMapMarkerActive(index) {
    let elmnt = document.getElementById(index);
    let previousElement = document.getElementsByClassName('sia-marker-active');
    if (previousElement.length > 0) {
      previousElement[0].classList.remove('sia-marker-active')
    }
    elmnt.classList.toggle('sia-marker-active');
    elmnt.scrollIntoView({ behavior: 'smooth', inline: 'start' });
  }

  onMapMarkerClick(index) {
    this.setMapMarkerActive(index);
    this.setActiveMarker(index)
  }

  onBbookButtonClick(event) {
    alert("YOU HAVE BOOKED " + event.target.id)
  }
}